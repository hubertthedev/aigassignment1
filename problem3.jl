### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 4302c5f2-1db3-402f-bff0-55996eadca58
using Pkg

# ╔═╡ cf838f70-7c45-4e82-a437-ba5b84ed4511
Pkg.activate(".")

# ╔═╡ c1935a28-0bb5-4e92-873e-b8418deea4a6
Pkg.add("BenchmarkTools")

# ╔═╡ bf4ed981-0286-4ba9-8679-6588c309ff12
using BenchmarkTools

# ╔═╡ c908c5b4-b16d-11eb-214c-b9b3470a9946
md"## Problem 3 : Constraint Satisfaction Problem"

# ╔═╡ c661ef01-dcbc-4e51-bdde-89fab214276a
domain = [1,2,3,4]

# ╔═╡ 1cce94ba-9343-4c44-b1ce-0b443099f7da
function assign()
	for statement = "Constraint Satisfaction Solved"
		
	
	 	x1 = rand(domain)
		x2 = rand(domain)
		x3 = 1
		x4 = rand(domain)
		x5 = rand(domain)
		x6 = rand(domain)
		x7 = rand(domain)
		
	
	
	#backtracking algorithm
if(x3 == 1 && x1 != x2 && x1 != x3 && x1 != x4 && x1 != x6 && x3 != x4 && x5 != x4 && x5 != x6 && x6 != x7 && x2 != x5)
	statement = "Constraint Satisfaction Solved"
			println(x1,x2,x3,x4,x5,x6,x7)
else
		statement = "error"
	end
	end
end

# ╔═╡ 160a80da-8315-40b0-9267-bdd278064330
assign()

# ╔═╡ Cell order:
# ╟─c908c5b4-b16d-11eb-214c-b9b3470a9946
# ╠═4302c5f2-1db3-402f-bff0-55996eadca58
# ╠═cf838f70-7c45-4e82-a437-ba5b84ed4511
# ╠═c1935a28-0bb5-4e92-873e-b8418deea4a6
# ╠═bf4ed981-0286-4ba9-8679-6588c309ff12
# ╠═c661ef01-dcbc-4e51-bdde-89fab214276a
# ╠═1cce94ba-9343-4c44-b1ce-0b443099f7da
# ╠═160a80da-8315-40b0-9267-bdd278064330

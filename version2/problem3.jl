### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 4302c5f2-1db3-402f-bff0-55996eadca58
using Pkg

# ╔═╡ cf838f70-7c45-4e82-a437-ba5b84ed4511
Pkg.activate(".")

# ╔═╡ c1935a28-0bb5-4e92-873e-b8418deea4a6
Pkg.add("BenchmarkTools")

# ╔═╡ bf4ed981-0286-4ba9-8679-6588c309ff12
using BenchmarkTools

# ╔═╡ 5d9f3e21-c4e8-4905-8719-4a409614a64a
using PlutoUI

# ╔═╡ c908c5b4-b16d-11eb-214c-b9b3470a9946
md"## Problem 3 : Constraint Satisfaction Problem"

# ╔═╡ c661ef01-dcbc-4e51-bdde-89fab214276a
@enum domain one two three four

# ╔═╡ 744af5e3-98d3-4d61-8003-44b1f8f5859f
mutable struct var
	name::String
	value::Union{Nothing,domain}
	not_allowed::Vector{domain}
	domainRestriction::Int64
end

# ╔═╡ 71b8e3f0-581b-4003-8970-4c7d74bbd7c0
struct numbers
var::Vector{var}
constraints::Vector{Tuple{var,var}}
end

# ╔═╡ d48f354b-1d33-4861-9c23-02064a60e712
num = rand(setdiff(Set([one,two,three,four]),Set([one,two])))

# ╔═╡ ddf1a3ef-afd4-4fa6-8806-40395acd00c5
function solve(pb::numbers, all_assignments)
	for current_var in pb.variables
		if current_var.domain_restriction_count==4
			return []
		else
			next_val = rand(setdiff(Set([one,two,three,four]), Set(current_var.not_allowed)))
			
			for current_constraint in pb.constraints
				if !((current_constraint[1] == current_var) || (current_constraint[2] == current_var))
					continue
				else
					if current_constraint[1]==current_var
						push!(current_constraint[2].not_allowed, next_val)
						current_constraint[2].domain_restriction_count +=1
					else
						push!(current_constraint[1].not_allowed, next_val)
						current_constraint[1].domain_restriction_count +=1
					end
				end
			end
			push!(all_assignments, current_var.name=>next_val)
		end
	end
	return all_assignments
end

# ╔═╡ 1397c08d-3a4b-48c1-8f35-a398b4d13ff7
x1 = var("x1",nothing,[],0)

# ╔═╡ 344549e1-1006-42f7-91f1-8a5ccf11f655
x2 = var("x2",nothing,[],0)

# ╔═╡ 1f046ff6-4dd8-4d9d-8883-e37800cadaf2
x3 = var("x3",nothing,[],0)

# ╔═╡ 20ff8965-2c14-4042-8ce1-f10a489d915d
x4 = var("x4",nothing,[],0)

# ╔═╡ 17b7b934-d798-4503-b96a-1a28815242d1
x5 = var("x5",nothing,[],0)

# ╔═╡ 4935b94b-02fe-471a-95c1-4c31db1bb682
x6 = var("x6",nothing,[],0)

# ╔═╡ 5b7ff5b2-46d6-4ec9-a786-0bbf85162c74
x7 = var("x7",nothing,[],0)

# ╔═╡ 22de0dd5-7183-4916-b96e-7ac0dfff2240
 csp= numbers([x1,x2,x3,x4,x5,x6,x7], [(x1,x2),(x1,x3),(x1,x4),(x1,x5),(x1,x6),(x2,x5),(x3,x4),(x4,x5),(x4,x6),(x5,x6),(x6,x7)])


# ╔═╡ 1cce94ba-9343-4c44-b1ce-0b443099f7da
function assign()
	
	for statement = "Constraint Satisfaction Solved"
		
	
	#backtracking algorithm
if(x3 == 1 && x1 != x2 && x1 != x3 && x1 != x4 && x1 != x6 && x3 != x4 && x5 != x4 && x5 != x6 && x6 != x7 && x2 != x5)

	statement = "Constraint Satisfaction Solved"
			println(x1,x2,x3,x4,x5,x6,x7)

else
		statement = "error"
	end
	
	end
end

# ╔═╡ 160a80da-8315-40b0-9267-bdd278064330
with_terminal() do
	assign()
end

# ╔═╡ ec963539-11dd-419c-9d8d-55b805856fdc


# ╔═╡ Cell order:
# ╟─c908c5b4-b16d-11eb-214c-b9b3470a9946
# ╠═4302c5f2-1db3-402f-bff0-55996eadca58
# ╠═cf838f70-7c45-4e82-a437-ba5b84ed4511
# ╠═c1935a28-0bb5-4e92-873e-b8418deea4a6
# ╠═bf4ed981-0286-4ba9-8679-6588c309ff12
# ╠═5d9f3e21-c4e8-4905-8719-4a409614a64a
# ╠═c661ef01-dcbc-4e51-bdde-89fab214276a
# ╠═744af5e3-98d3-4d61-8003-44b1f8f5859f
# ╠═71b8e3f0-581b-4003-8970-4c7d74bbd7c0
# ╠═d48f354b-1d33-4861-9c23-02064a60e712
# ╠═ddf1a3ef-afd4-4fa6-8806-40395acd00c5
# ╠═1397c08d-3a4b-48c1-8f35-a398b4d13ff7
# ╠═344549e1-1006-42f7-91f1-8a5ccf11f655
# ╠═1f046ff6-4dd8-4d9d-8883-e37800cadaf2
# ╠═20ff8965-2c14-4042-8ce1-f10a489d915d
# ╠═17b7b934-d798-4503-b96a-1a28815242d1
# ╠═4935b94b-02fe-471a-95c1-4c31db1bb682
# ╠═5b7ff5b2-46d6-4ec9-a786-0bbf85162c74
# ╠═22de0dd5-7183-4916-b96e-7ac0dfff2240
# ╠═1cce94ba-9343-4c44-b1ce-0b443099f7da
# ╠═160a80da-8315-40b0-9267-bdd278064330
# ╠═ec963539-11dd-419c-9d8d-55b805856fdc

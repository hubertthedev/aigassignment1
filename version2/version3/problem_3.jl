### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 1d785a90-b2df-11eb-16a0-5fcae393255d
using Pkg;

# ╔═╡ 74b9d234-90ed-41a6-81dc-099d98bba54f
Pkg.activate("Project.toml")

# ╔═╡ a8fafe07-9eaf-4458-930e-f511863087d3
@enum numDomain one two three four

# ╔═╡ ad8c8c0a-2119-4652-b06f-c63f8394489a
mutable struct Variables
	name::String
	value::Union{Nothing,numDomain}
	not_allowed::Vector{numDomain}
	domain_restriction_count::Int64
end

# ╔═╡ e9b65680-2940-448e-a6d9-25e7670ec3fb
struct numberCSP
	variables::Vector{Variables}
	constraints::Vector{Tuple{Variables, Variables}}
end

# ╔═╡ 375c030d-5a1f-4898-b94c-327796c0bc87
numbers =rand(setdiff(Set([one,two,three,four]), Set([one,two])))

# ╔═╡ 95fa0977-087f-4c14-94ce-d27ee04a3247
function solve_csp(pb::numberCSP, all_assignments)
	for current_var in pb.variables
		if current_var.domain_restriction_count==4
			return []
		else
			next_val = rand(setdiff(Set([one,two,three,four]), Set(current_var.not_allowed)))
			
			for current_constraint in pb.constraints
				if !((current_constraint[1] == current_var) || (current_constraint[2] == current_var))
					continue
				else
					if current_constraint[1]==current_var
						push!(current_constraint[2].not_allowed, next_val)
						current_constraint[2].domain_restriction_count +=1
					else
						push!(current_constraint[1].not_allowed, next_val)
						current_constraint[1].domain_restriction_count +=1
					end
				end
			end
			push!(all_assignments, current_var.name=>next_val)
		end
	end
	return all_assignments
end

# ╔═╡ 694f8369-c2d7-4557-abe9-e94961359ade
x1 = Variables("x1",nothing,[],0)

# ╔═╡ cca62933-75d5-4d60-85c8-fcf5db6c18be
x2 = Variables("x2",nothing,[],0)

# ╔═╡ 4539b944-e98f-4994-961f-c1db560e2e2a
x3 = Variables("x3",nothing,[one],0)

# ╔═╡ 45109e94-d24e-4ae3-b505-ffb6b1d0c939
x4 = Variables("x4",nothing,[],0)

# ╔═╡ 66650f0c-afae-44e9-aa59-1a93e79c94f9
x5 = Variables("x5",nothing,[],0)

# ╔═╡ 8f30d57c-5445-47ae-bf04-0a08b0bfe025
x6 = Variables("x6",nothing,[],0)

# ╔═╡ 61b7cb29-bcd7-4112-aa97-7dfc334789d4
x7 = Variables("x7",nothing,[],0)

# ╔═╡ 39bac1cb-3547-4651-a98e-59c3d6cb0f33
problem = numberCSP([x1,x2,x3,x4,x5,x6,x7], [(x1,x2),(x1,x3),(x1,x4),(x1,x5),(x1,x6),(x2,x5),(x3,x4),(x4,x5),(x4,x6),(x5,x6),(x6,x7)])

# ╔═╡ 04529c79-ba5a-4fc1-93e5-1b7f969bd694
solve_csp(problem,[])

# ╔═╡ Cell order:
# ╠═1d785a90-b2df-11eb-16a0-5fcae393255d
# ╠═74b9d234-90ed-41a6-81dc-099d98bba54f
# ╠═a8fafe07-9eaf-4458-930e-f511863087d3
# ╠═ad8c8c0a-2119-4652-b06f-c63f8394489a
# ╠═e9b65680-2940-448e-a6d9-25e7670ec3fb
# ╠═375c030d-5a1f-4898-b94c-327796c0bc87
# ╠═95fa0977-087f-4c14-94ce-d27ee04a3247
# ╠═694f8369-c2d7-4557-abe9-e94961359ade
# ╠═cca62933-75d5-4d60-85c8-fcf5db6c18be
# ╠═4539b944-e98f-4994-961f-c1db560e2e2a
# ╠═45109e94-d24e-4ae3-b505-ffb6b1d0c939
# ╠═66650f0c-afae-44e9-aa59-1a93e79c94f9
# ╠═8f30d57c-5445-47ae-bf04-0a08b0bfe025
# ╠═61b7cb29-bcd7-4112-aa97-7dfc334789d4
# ╠═39bac1cb-3547-4651-a98e-59c3d6cb0f33
# ╠═04529c79-ba5a-4fc1-93e5-1b7f969bd694

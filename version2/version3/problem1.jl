### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ 2440ff7c-c008-4711-876d-255d39823b06
using Pkg; Pkg.add("DataStructures");using DataStructures
#using Datastructure.jl to call priority queue

# ╔═╡ b253f524-d27a-4cf2-9462-c9eb9f3b36d9
using PlutoUI

# ╔═╡ b7fa5473-43c7-43a7-980b-d60ab0abded7
md"## Class Structure"

# ╔═╡ 03340a40-af87-11eb-201b-43c2ac9574af
struct States
	name::String
	Position::String
	trash::Vector{Int64}
	Remaining::Int64
	
end

# ╔═╡ 7fa8b977-80e6-43f3-a5d6-3c373a948a5e
md"## Actions and Costs"

# ╔═╡ 571100a8-63d4-4ec0-972f-5ad7040d23a5
struct Action
	name::String
	cost::Int64
end

# ╔═╡ 0545b88f-844e-4d1d-92fd-451b46f7baf1
co=Action("CleanOffice", 5)

# ╔═╡ b8fe251a-3081-4065-a8e5-4245f8c53f52
mw=Action("MoveWest",3)

# ╔═╡ 2f6107c6-7bd9-46a8-aa9e-1128809d0ffa
me=Action("MoveEast",3)

# ╔═╡ 05581808-9a40-4670-8055-930d1feac878
re=Action("Remain",1)

# ╔═╡ de919d9f-813c-471d-a415-8f0fa290d937
md"## States From Initial config"

# ╔═╡ b3adf2f3-829a-4402-a580-626ef767afa1


# ╔═╡ 33240877-d226-42e5-b23d-433e7e80688c
st1 = States("Initial","C1",[1,3,2,1],7)

# ╔═╡ e5575894-f733-4520-986b-f36b7a3b0197
c11=States("c11","C1",[1,2,2,1],6)
#clean up in C1

# ╔═╡ 95dff702-8296-4c29-b373-2c22d5959370
c12=States("c12","C1",[1,1,2,1],5)

# ╔═╡ ff96a191-6984-47a9-98c6-eda5e16cfa4e
c13=States("c13","C1",[1,0,2,1],4)
# c1 cleanup complete

# ╔═╡ 3f80de38-bef8-4eda-a4a2-1660af1b1ee6
c14=States("c14","W",[1,0,2,1],4)
#move left to W office

# ╔═╡ fe76a77f-09c7-4bc6-b6b1-3099c94187b6
c15=States("c15","W",[0,0,2,1],3)
#clean up complete

# ╔═╡ 7e27a300-4a39-4155-a5b7-be4bd0898f12
c16=States("c16","W",[0,0,2,1],3)
#remain in office W 

# ╔═╡ 2fba52f5-1546-4b07-903b-5dc3c35f85ec
c17=States("c17","C1",[0,0,2,1],3)
#move right to office C1

# ╔═╡ 0f447e8e-3329-448b-a625-f06a0231de72
c18=States("c18","C2",[0,0,2,1],3)
#arrival in C2

# ╔═╡ b3eb3d88-24fc-4e68-8d1d-49b33d0f2456
c19=States("c19","C2",[0,0,1,1],2)
#clean up in C2

# ╔═╡ 0cb9ce15-753d-4315-9697-537cb7c48928
c110=States("C110","C2",[0,0,0,1],1)
#clean up complete in C2

# ╔═╡ 1dc8cd60-6f51-4bc0-841c-03f13d01ae47
c111=States("C111","E",[0,0,0,1],1)
#arrival in E 

# ╔═╡ 506b5f15-522b-4b47-a043-b274e07c0cf6
c112=States("C112","E",[0,0,0,0],0)

# ╔═╡ 78869ae6-3482-492f-a3a4-708d9a09c5b8
c113=States("c113","E",[0,0,0,0],0)
#remain in office E, goal state

# ╔═╡ f5b8cd66-c93e-498c-a5ca-faf3e770bdff
md"### States of Office W from Initial State"

# ╔═╡ c06c09e5-e531-4d9a-afbb-333493ed5631
w1=States("w1","W",[1,3,2,1],7)

# ╔═╡ 9d59b134-ab89-46de-b1bf-91fb341ef290
w2=States("W2","W",[0,3,2,1],6)

# ╔═╡ 83d9e458-346f-420d-80c3-8564c3372eb3
w3=States("W3","W",[0,3,2,1],6)
#remain in W after cleaning

# ╔═╡ 263ba1f1-6689-4e3c-adfd-4be36188ec0c
w4=States("W4","C1",[0,3,2,1],6)

# ╔═╡ 4098d323-cc89-4ec7-83c5-1d3cd9b7e563
w5=States("W5","C1",[0,2,2,1],5)

# ╔═╡ 052ee1f6-7a0d-4465-b577-29558d096a68
w6=States("W6","C1",[0,1,2,1],4)

# ╔═╡ 30442afd-1eaf-4c7e-9271-fa9c8cc5e44a
w7=States("W7","C1",[0,0,2,1],3)
#this state can be mapped to c18

# ╔═╡ e258ad8b-2961-4d24-b7fe-27a334638cb2
md"#### States of Office of C2"

# ╔═╡ af196d5d-936a-468e-9083-bcf1346959f2
c21=States("InitialC2","C2",[1,3,2,1],7)

# ╔═╡ 009c5c7d-bdde-4a45-b60a-1e8b67bd73e7
c22=States("C22","C2",[1,3,1,1],6)

# ╔═╡ f201b740-8a83-4bb9-b881-2570a3ce0315
c23=States("C23","C2",[1,3,0,1],5)

# ╔═╡ a13b7ee2-210a-4775-9316-34dcfd56022c
c24=States("C24","C1",[1,3,0,1],5)
#Move left to C1

# ╔═╡ 6618f3d7-23f4-4543-9279-b11e356d494a
c25=States("C25","C1",[1,2,0,1],4)


# ╔═╡ 94f5ccdc-90e9-4001-8465-85c6201c865d
c26=States("C26","C1",[1,1,0,1],3)


# ╔═╡ 6cc4f172-0070-4554-9186-449a79423cfb
c27=States("C27","C1",[1,0,0,1],2)


# ╔═╡ 02ba7612-7b3d-41f3-9f5c-dff5bbae52c9
c28=States("C28","W",[1,0,0,1],2)

# ╔═╡ 5680af4f-e599-453b-8677-8a72f07d16e3
c29=States("C29","W",[0,0,0,1],1)
#cleaning W

# ╔═╡ 106e838b-0724-4bd0-a3de-a2db13a28f28
c210=States("C210","W",[0,0,0,1],1)
#remain in W

# ╔═╡ 93efe0d5-16e5-4942-a7df-be4b2f68a9a3
c211=States("C210","C1",[0,0,0,1],1)
#move right to C1, this can be mapped to C110 

# ╔═╡ cb31d1b5-2bbd-4ba9-84a0-3290df2c06cf
c2A=States("C2A","E",[1,3,0,1],5)

# ╔═╡ 18ffcd69-6106-4b3b-9f9a-797aef910862
c2B=States("C2B","E",[1,3,0,0],4)

# ╔═╡ 080d77f7-3356-483b-aa49-0fd0387d0d4e
c2C=States("C2C","E",[1,3,0,0],4)
#remain in E

# ╔═╡ 2f06b20a-e6cf-453d-90a3-92c51e0c36d9
c2D=States("C2D","C2",[1,3,0,0],4)

# ╔═╡ 904bc379-f9e4-47a4-817f-ddf4b5d28c6a
c2E=States("C2E","C1",[1,3,0,0],4)
#move to C1

# ╔═╡ a62ed4cd-9cc6-434f-b955-d599466beb8b
c2F=States("C2F","C1",[1,2,0,0],3)

# ╔═╡ 1b4dceb1-7c06-4093-a838-7bf2ee3984f9
c2G=States("C2G","C1",[1,1,0,0],2)

# ╔═╡ dbbf3cae-24bc-4d71-b28b-b5029b9372b2
c2H=States("C2H","C1",[1,0,0,0],1)

# ╔═╡ 4ec06024-bb05-42ff-8c50-1e0182c85cac
c2I=States("C2I","W",[1,0,0,0],1)

# ╔═╡ 822b316f-eb9e-4305-b804-0c4d215361fc
c2J=States("C2J","W",[0,0,0,0],0)
#clean W

# ╔═╡ 85afb909-ab70-4555-bfe9-36cf4f4bc7e4
c2K=States("C2K","W",[0,0,0,0],0)
#remain in C

# ╔═╡ 1ff87aac-69c6-4dc6-ab00-874a06be468b
md"##### states of E"

# ╔═╡ c99e65e7-7115-4284-b964-3980673a4aa1
e1=States("InitialE","E",[1,3,2,1],7)

# ╔═╡ dc4232c1-0c32-405d-a84a-069b04451711
e2=States("E2","E",[1,3,2,0],6)

# ╔═╡ 884e881e-37a6-4376-87f2-6b96767c5696
e3=States("E3","E",[1,3,2,0],6)

# ╔═╡ d4513045-1f65-487d-b05a-55922a1c60fa
e4=States("E4","C2",[1,3,2,0],6)

# ╔═╡ 50c189f5-9634-4c93-9757-3d0c1e84ea68
e5=States("E5","C2",[1,3,1,0],5)

# ╔═╡ fc824dde-712d-4a65-b3e9-07f6d0279f6a
md"## Transition Model Map of States"

# ╔═╡ 60c6b66a-09a0-4320-b56d-a818795a1bbb
Transmodel = Dict()

# ╔═╡ c9d894b5-8547-4e52-aa39-4dc752b9d86c
push!(Transmodel, st1=> [(c11,co),(w1,mw),(c21,me),(e1,me)])

# ╔═╡ c2200423-1f65-4d18-aa96-ce6edf41f53b
push!(Transmodel, c11=>[(c12,co)])

# ╔═╡ 724aa061-ec6d-4bb0-8e6c-cbbd46c986c7
push!(Transmodel, c12=>[(c13,co)])

# ╔═╡ 9a85f3fd-3d31-44e9-8886-b1f52c2b3dea
push!(Transmodel, c13=>[(c14,mw)])

# ╔═╡ d68e6761-243b-4ec2-aa9b-d979992295e0
push!(Transmodel, c14=>[(c15,co)])

# ╔═╡ fc2f1810-3b0f-40c4-896a-8fd1edf60805
push!(Transmodel, c15=>[(c16,re)])

# ╔═╡ d320dedb-49ca-4174-b82f-d71540669f0d
push!(Transmodel, c16=>[(c17,me)])

# ╔═╡ 16af74e1-5176-4f93-9458-f9109ffd5e79
push!(Transmodel, c17=>[(c18,me)])

# ╔═╡ 9016c967-2721-4b5c-b5ed-a03c529a186f
push!(Transmodel, c18=>[(c19,co)])

# ╔═╡ c79d965a-b049-431c-8318-55eec97fadbc
push!(Transmodel, c19=>[(c110,co)])

# ╔═╡ 441868ce-8e81-4a32-9103-f57dd849c20e
push!(Transmodel, c110=>[(c111,me)])

# ╔═╡ 2b0ed267-f7d2-43ff-9132-0303cc739e72
push!(Transmodel, c111=>[(c112,co)])

# ╔═╡ 942357b8-eb18-42aa-9c22-8ef081eb79d9
push!(Transmodel, w1=>[(w2,co)])

# ╔═╡ aeace9a6-a7e1-41d5-baf3-2311bc745e25
push!(Transmodel, w2=>[(w3,re)])

# ╔═╡ 0e2e82c1-f0f2-47eb-9c1b-fdc16b1f9e43
push!(Transmodel, w3=>[(w4,me)])

# ╔═╡ 709e58df-a070-484e-91cc-6392f7d0a1bf
push!(Transmodel, w4=>[(w5,co)])

# ╔═╡ f5e13227-c9fb-4b21-afd4-c0f88a130253
push!(Transmodel, w5=>[(w6,co)])

# ╔═╡ cc25dad7-6665-45cb-b24e-4fc145767b02
push!(Transmodel, w6=>[(w7,co)])

# ╔═╡ 7f21025d-9df7-45fd-a0c5-8dc5b38ee473
push!(Transmodel, w7=>[(c18,me)])

# ╔═╡ 6252e099-62e7-430e-92c4-b8b11a6560d6
push!(Transmodel, c21=>[(c22,co)])

# ╔═╡ e365e4d1-0426-4a86-84f7-54ab12c70169
push!(Transmodel, c22=>[(c23,co)])

# ╔═╡ b1189290-9fed-4f65-a1e7-a8f231347fdd
push!(Transmodel, c23=>[(c24,mw),(c2A,me)])

# ╔═╡ 600738cc-fd75-4b3e-ad5b-b1a72e1a9421
push!(Transmodel, c24=>[(c25,co)])

# ╔═╡ 2815d6c9-3064-4b13-b57f-7c8feb86bbf1
push!(Transmodel, c25=>[(c26,co)])

# ╔═╡ fed07c96-ad79-4517-8665-599d047a5fd3
push!(Transmodel, c26=>[(c27,co)])

# ╔═╡ 65451194-cbdb-4033-8657-048bd4b1ae50
push!(Transmodel, c27=>[(c28,mw)])

# ╔═╡ 0aac56ec-85b7-4675-8f91-bce2df7e31db
push!(Transmodel, c28=>[(c29,co)])

# ╔═╡ c213abd5-e6d4-4c72-b0d0-598b20952b68
push!(Transmodel, c29=>[(c210,re)])

# ╔═╡ 4b568718-c9f0-4e05-a814-0d66d3ae8efa
push!(Transmodel, c210=>[(c211,me)])

# ╔═╡ f70dcbea-5884-4ed5-8b00-b448320121c0
push!(Transmodel, c211=>[(c110,me)])

# ╔═╡ 2faeb21c-1b3e-429e-bf93-bcf82b8dfec7
push!(Transmodel, c2A=>[(c2B,co)])

# ╔═╡ fdce8512-6912-4f77-903b-ee5d58f9d437
push!(Transmodel, c2B=>[(c2C,mw)])

# ╔═╡ de2b94ae-2b32-4899-b09b-70b6cd33bbfc
push!(Transmodel, c2C=>[(c2D,mw)])

# ╔═╡ 08511a7a-cb8d-4969-b72f-43c00ec27d62
push!(Transmodel, c2D=>[(c2E,co)])

# ╔═╡ 6b06ff9c-49e4-4682-ac6b-efa4cdca8bec
push!(Transmodel, c2E=>[(c2F,co)])

# ╔═╡ a7e2b316-c95e-425a-895c-56235a53e224
push!(Transmodel, c2F=>[(c2G,co)])

# ╔═╡ 9733623f-61ac-494c-89df-2813af34217f
push!(Transmodel,c2G=>[(c2H,mw)])

# ╔═╡ 5d3208e1-1b71-48f9-a7ca-b13743cc471d
push!(Transmodel, c2H=>[(c2I,co)])

# ╔═╡ 678d09b9-5165-4050-85e5-4d1133cb0ff7
push!(Transmodel, c2I=>[(c2J,re)])

# ╔═╡ 6cbc3ab6-76a1-4643-b260-e604d605e9d8
push!(Transmodel, e1=>[(e2,co)])

# ╔═╡ eb099f13-1c61-484b-a468-526e39f3503e
push!(Transmodel, e2=>[(e3,re)])

# ╔═╡ 11a7ba7a-64e7-4b58-9eb4-ffacde82ee34
push!(Transmodel, e3=>[(e4,mw)])

# ╔═╡ 43736d31-84fe-41f9-9738-1f3cf3505e2f
push!(Transmodel, e4=>[(e5,co)])

# ╔═╡ 0b10c36e-889b-42b2-8951-64acbac99ce6
push!(Transmodel, e5=>[(c2D,mw)])

# ╔═╡ 21b6feb8-98d4-4783-a6a3-4649ff9c94b8


# ╔═╡ bde04c7e-c824-4ca0-8c19-8dd918cf262f
startNode=st1
#initialising start node to parent node

# ╔═╡ 61275233-7cc1-4d09-acfb-42a7af84f29e
goalNode=c2J

# ╔═╡ 6740ff71-e3f2-46df-a8c3-e5764b1c4d18

if st1 == c2J
	println("found goal")
	else
		println("no goal found")
end

# ╔═╡ d7b82f58-81f3-4c65-a7b2-145bdb049035
function evaluat(Action,States)
	g=Action.cost
	h=States.Remaining
	f=0
	return f=g+h
end

# ╔═╡ fbb5e2cd-98b5-433c-94ff-21154e99261e
evaluat(re,goalNode)

# ╔═╡ e93e5ebc-c111-4fa7-bbf2-57f3c5a500d7
eval(st1)

# ╔═╡ f896669d-8ec4-40a0-877b-8e8b222189ca
function a_search(Transmodel, start, end_node)
	start=St1 #set start node to St1
    end_node=goalNode #set goal node to corresponding goal states
	open=PriorityQueue{Remaining,State}
	closed=PriorityQueue{Remaining, State}() # creste priority queue that sets according to number of remaining dirt 
	while open>0
		push!(Transmodel[0], current_node)
		current_node==start
		with_terminal() do
		if current_node==goalNode
				println("found goal")
			else
				println("no goal found")
			end
			pop!(Transmodel,St1)#remove
			enqueue!(closed, St1.Remaning, St1)
				current_node==child
				child.f=child.g+child.h
				while child != currrent_node
					enqueue!(closed, child.Remaining)
				end
		end
	end
end			


# ╔═╡ 3779e015-576c-4b02-aa0b-42a4b8fe068c
a_search(Transmodel,st1,c2J)

# ╔═╡ Cell order:
# ╠═b7fa5473-43c7-43a7-980b-d60ab0abded7
# ╠═03340a40-af87-11eb-201b-43c2ac9574af
# ╠═7fa8b977-80e6-43f3-a5d6-3c373a948a5e
# ╠═571100a8-63d4-4ec0-972f-5ad7040d23a5
# ╠═0545b88f-844e-4d1d-92fd-451b46f7baf1
# ╠═b8fe251a-3081-4065-a8e5-4245f8c53f52
# ╠═2f6107c6-7bd9-46a8-aa9e-1128809d0ffa
# ╠═05581808-9a40-4670-8055-930d1feac878
# ╠═de919d9f-813c-471d-a415-8f0fa290d937
# ╠═b3adf2f3-829a-4402-a580-626ef767afa1
# ╠═33240877-d226-42e5-b23d-433e7e80688c
# ╠═e5575894-f733-4520-986b-f36b7a3b0197
# ╠═95dff702-8296-4c29-b373-2c22d5959370
# ╠═ff96a191-6984-47a9-98c6-eda5e16cfa4e
# ╠═3f80de38-bef8-4eda-a4a2-1660af1b1ee6
# ╠═fe76a77f-09c7-4bc6-b6b1-3099c94187b6
# ╠═7e27a300-4a39-4155-a5b7-be4bd0898f12
# ╠═2fba52f5-1546-4b07-903b-5dc3c35f85ec
# ╠═0f447e8e-3329-448b-a625-f06a0231de72
# ╠═b3eb3d88-24fc-4e68-8d1d-49b33d0f2456
# ╠═0cb9ce15-753d-4315-9697-537cb7c48928
# ╠═1dc8cd60-6f51-4bc0-841c-03f13d01ae47
# ╠═506b5f15-522b-4b47-a043-b274e07c0cf6
# ╠═78869ae6-3482-492f-a3a4-708d9a09c5b8
# ╠═f5b8cd66-c93e-498c-a5ca-faf3e770bdff
# ╠═c06c09e5-e531-4d9a-afbb-333493ed5631
# ╠═9d59b134-ab89-46de-b1bf-91fb341ef290
# ╠═83d9e458-346f-420d-80c3-8564c3372eb3
# ╠═263ba1f1-6689-4e3c-adfd-4be36188ec0c
# ╠═4098d323-cc89-4ec7-83c5-1d3cd9b7e563
# ╠═052ee1f6-7a0d-4465-b577-29558d096a68
# ╠═30442afd-1eaf-4c7e-9271-fa9c8cc5e44a
# ╠═e258ad8b-2961-4d24-b7fe-27a334638cb2
# ╠═af196d5d-936a-468e-9083-bcf1346959f2
# ╠═009c5c7d-bdde-4a45-b60a-1e8b67bd73e7
# ╠═f201b740-8a83-4bb9-b881-2570a3ce0315
# ╠═a13b7ee2-210a-4775-9316-34dcfd56022c
# ╠═6618f3d7-23f4-4543-9279-b11e356d494a
# ╠═94f5ccdc-90e9-4001-8465-85c6201c865d
# ╠═6cc4f172-0070-4554-9186-449a79423cfb
# ╠═02ba7612-7b3d-41f3-9f5c-dff5bbae52c9
# ╠═5680af4f-e599-453b-8677-8a72f07d16e3
# ╠═106e838b-0724-4bd0-a3de-a2db13a28f28
# ╠═93efe0d5-16e5-4942-a7df-be4b2f68a9a3
# ╠═cb31d1b5-2bbd-4ba9-84a0-3290df2c06cf
# ╠═18ffcd69-6106-4b3b-9f9a-797aef910862
# ╠═080d77f7-3356-483b-aa49-0fd0387d0d4e
# ╠═2f06b20a-e6cf-453d-90a3-92c51e0c36d9
# ╠═904bc379-f9e4-47a4-817f-ddf4b5d28c6a
# ╠═a62ed4cd-9cc6-434f-b955-d599466beb8b
# ╠═1b4dceb1-7c06-4093-a838-7bf2ee3984f9
# ╠═dbbf3cae-24bc-4d71-b28b-b5029b9372b2
# ╠═4ec06024-bb05-42ff-8c50-1e0182c85cac
# ╠═822b316f-eb9e-4305-b804-0c4d215361fc
# ╠═85afb909-ab70-4555-bfe9-36cf4f4bc7e4
# ╠═1ff87aac-69c6-4dc6-ab00-874a06be468b
# ╠═c99e65e7-7115-4284-b964-3980673a4aa1
# ╠═dc4232c1-0c32-405d-a84a-069b04451711
# ╠═884e881e-37a6-4376-87f2-6b96767c5696
# ╠═d4513045-1f65-487d-b05a-55922a1c60fa
# ╠═50c189f5-9634-4c93-9757-3d0c1e84ea68
# ╠═fc824dde-712d-4a65-b3e9-07f6d0279f6a
# ╠═60c6b66a-09a0-4320-b56d-a818795a1bbb
# ╠═c9d894b5-8547-4e52-aa39-4dc752b9d86c
# ╠═c2200423-1f65-4d18-aa96-ce6edf41f53b
# ╠═724aa061-ec6d-4bb0-8e6c-cbbd46c986c7
# ╠═9a85f3fd-3d31-44e9-8886-b1f52c2b3dea
# ╠═d68e6761-243b-4ec2-aa9b-d979992295e0
# ╠═fc2f1810-3b0f-40c4-896a-8fd1edf60805
# ╠═d320dedb-49ca-4174-b82f-d71540669f0d
# ╠═16af74e1-5176-4f93-9458-f9109ffd5e79
# ╠═9016c967-2721-4b5c-b5ed-a03c529a186f
# ╠═c79d965a-b049-431c-8318-55eec97fadbc
# ╠═441868ce-8e81-4a32-9103-f57dd849c20e
# ╠═2b0ed267-f7d2-43ff-9132-0303cc739e72
# ╠═942357b8-eb18-42aa-9c22-8ef081eb79d9
# ╠═aeace9a6-a7e1-41d5-baf3-2311bc745e25
# ╠═0e2e82c1-f0f2-47eb-9c1b-fdc16b1f9e43
# ╠═709e58df-a070-484e-91cc-6392f7d0a1bf
# ╠═f5e13227-c9fb-4b21-afd4-c0f88a130253
# ╠═cc25dad7-6665-45cb-b24e-4fc145767b02
# ╠═7f21025d-9df7-45fd-a0c5-8dc5b38ee473
# ╠═6252e099-62e7-430e-92c4-b8b11a6560d6
# ╠═e365e4d1-0426-4a86-84f7-54ab12c70169
# ╠═b1189290-9fed-4f65-a1e7-a8f231347fdd
# ╠═600738cc-fd75-4b3e-ad5b-b1a72e1a9421
# ╠═2815d6c9-3064-4b13-b57f-7c8feb86bbf1
# ╠═fed07c96-ad79-4517-8665-599d047a5fd3
# ╠═65451194-cbdb-4033-8657-048bd4b1ae50
# ╠═0aac56ec-85b7-4675-8f91-bce2df7e31db
# ╠═c213abd5-e6d4-4c72-b0d0-598b20952b68
# ╠═4b568718-c9f0-4e05-a814-0d66d3ae8efa
# ╠═f70dcbea-5884-4ed5-8b00-b448320121c0
# ╠═2faeb21c-1b3e-429e-bf93-bcf82b8dfec7
# ╠═fdce8512-6912-4f77-903b-ee5d58f9d437
# ╠═de2b94ae-2b32-4899-b09b-70b6cd33bbfc
# ╠═08511a7a-cb8d-4969-b72f-43c00ec27d62
# ╠═6b06ff9c-49e4-4682-ac6b-efa4cdca8bec
# ╠═a7e2b316-c95e-425a-895c-56235a53e224
# ╠═9733623f-61ac-494c-89df-2813af34217f
# ╠═5d3208e1-1b71-48f9-a7ca-b13743cc471d
# ╠═678d09b9-5165-4050-85e5-4d1133cb0ff7
# ╠═6cbc3ab6-76a1-4643-b260-e604d605e9d8
# ╠═eb099f13-1c61-484b-a468-526e39f3503e
# ╠═11a7ba7a-64e7-4b58-9eb4-ffacde82ee34
# ╠═43736d31-84fe-41f9-9738-1f3cf3505e2f
# ╠═0b10c36e-889b-42b2-8951-64acbac99ce6
# ╠═21b6feb8-98d4-4783-a6a3-4649ff9c94b8
# ╠═2440ff7c-c008-4711-876d-255d39823b06
# ╠═bde04c7e-c824-4ca0-8c19-8dd918cf262f
# ╠═61275233-7cc1-4d09-acfb-42a7af84f29e
# ╠═b253f524-d27a-4cf2-9462-c9eb9f3b36d9
# ╠═6740ff71-e3f2-46df-a8c3-e5764b1c4d18
# ╠═d7b82f58-81f3-4c65-a7b2-145bdb049035
# ╠═fbb5e2cd-98b5-433c-94ff-21154e99261e
# ╠═e93e5ebc-c111-4fa7-bbf2-57f3c5a500d7
# ╠═f896669d-8ec4-40a0-877b-8e8b222189ca
# ╠═3779e015-576c-4b02-aa0b-42a4b8fe068c

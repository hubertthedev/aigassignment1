tic = [[" ", " ", " "],
         [" ", " ", " "],
         [" ", " ", " "]]


player1 = "X"
player2 = "O"

player1_win = -1
draw = 0
player2_win = 1

function displayGame(tic)
    println("    1   2   3")
    println(" 1  ", join(tic[1], " | "))
    println("   ------------")
    println(" 2  ", join(tic[2], " | "))
    println("   ------------")
    println(" 3  ", join(tic[3], " | "))
end

function viewAvailable(tic)
    available_cells = []
    for i in 1:3, j in 1:3
        if tic[i][j] == " "
            push!(available_cells, (i, j))
        end
    end
    return available_cells
end

function check_win(tic)

    # Rows
    for i in 1:3
        if (tic[i][1] == tic[i][2] == tic[i][3]) && (tic[i][1] in [player1, player2])
            if (tic[i][1] == player1)
                return player1_win
            end
            return player2_win
        end
    end

    # Column
    for i in 1:3
        if (tic[1][i] == tic[2][i] == tic[3][i]) && (tic[1][i] in [player1, player2])
            if (tic[1][i] == player1)
                return player1_win
            end
            return player2_win
        end
    end

    # Principal diagonal
    if (tic[1][1] == tic[2][2] == tic[3][3]) && (tic[1][1] in [player1, player2])
        if (tic[1][1] == player1)
            return player1_win
        end
        return player2_win
    end

    # Other diag
    if (tic[1][3] == tic[2][2] == tic[3][1]) && (tic[1][3] in [player1, player2])
        if (tic[1][3] == player1)
            return player1_win
        end
        return player2_win
    end

    # draw
    if length(viewAvailable(tic)) == 0
        return draw
    end

    # No one wins, game still goes on
    return nothing
end

function make_move!(tic, player, (x, y))
    tic[x][y] = player
end

function worst_move(tic)
    worst_score = Inf

    worst_pos = (0, 0)

    for i in 1:3, j in 1:3
        if tic[i][j] == " "

            tic[i][j] = player2
            
            score = minimax(tic,neg_inf,inf, 0, true)

            
            tic[i][j] = " "

            if score < worst_score
                worst_score = score
                worst_pos = (i, j)
            end
        end
    end
    return worst_pos
end

function ai_move!(tic, difficulty)
    if difficulty == "1"
        pos = rand(viewAvailable(tic))
        make_move!(tic, player2, pos)
    elseif difficulty == "2"
        if rand() < 0.3
            make_move!(tic, player2, best_move(tic))
        else            
            pos = rand(viewAvailable(tic))
            make_move!(tic, player2, pos)
        end
    elseif difficulty == "3"
        if rand() < 0.7
            make_move!(tic, player2, best_move(tic))
        else            
            pos = rand(viewAvailable(tic))
            make_move!(tic, player2, pos)
        end
    end
    
end

function minimax(tic,alpha,beta, depth, is_maximising)
    result = check_win(tic)

    if result !== nothing
        return result
    elseif is_maximising

        best_score = -Inf

        for i in 1:3, j in 1:3
            if tic[i][j] == " "

                tic[i][j] = player2

                score = minimax(tic, alpha,beta,depth + 1, false)
                if (max > best_score)
                tic[i][j] = " "
                best_score = max(score, best_score)
                end

                if(best_score>alpha)
                    alpha = best_score
                end
                if(alpha>=beta)
                    print("pruned")
                end

            end
        end

        return best_score        
    else        
        best_score = Inf

        for i in 1:3, j in 1:3
            if tic[i][j] == " "

                tic[i][j] = player1

                score = minimax(tic,alpha,beta, depth + 1, true)
                if (max > best_score)
                    tic[i][j] = " "
                    best_score = max(score, best_score)
                end
    
                    if(best_score>alpha)
                        alpha = best_score
                    end
                    if(alpha>=beta)
                        print("pruned")
                    end
                
            end
        end

        return best_score   
    end

end

function best_move(tic)
    best_score = -Inf

    best_pos = (0, 0)

    for i in 1:3, j in 1:3
        if tic[i][j] == " "

            tic[i][j] = player2
            
            score = minimax(tic, alpha,beta,0, false)

            
            tic[i][j] = " "

            if score > best_score
                best_score = score
                best_pos = (i, j)
            end
        end
    end
    return best_pos
end

function move_input(tic, prompt, params)

    print(prompt)
    txt = split(readline())

    if txt == ["exit"]
        exit()
    end

    if length(txt) == params && txt[1] in string.(collect(1:3)) && txt[2] in string.(collect(1:3)) && (tic[parse(Int, txt[1])][parse(Int, txt[2])] == " ")
        return txt
    else
        println("Invalid Entry")
        move_input(tic, prompt, params)
    end
end

function generalised_input(prompt, same_line, choices, params)
    same_line ? print(prompt) : println(prompt)

    txt = split(lowercase(readline()))

    if txt == ["exit"]
        exit()
    end

    if length(txt) == params && txt[1] in choices
        return txt
    else
        println("Invalid Entry")
        generalised_input(prompt, same_line, choices, params)
    end
end

function singleplayer(tic, (player1, player2), (player1_win, player2_win, draw))
    difficulty = """
    Choose a difficulty level:
    
    1 Easy
    2 Medium
    3 Hard 
    
    """

    println(difficulty)
    difficulty = generalised_input("Difficulty: ", true, ["1", "2", "3"], 1)[1]

    println("Player is X, AI is O.Human goes first")
    displayGame(tic)

    while true

        txt = move_input(tic, "Enter row and column seperated by space: ", 2)

        make_move!(tic, player1, (parse(Int, txt[1]), parse(Int, txt[2])))

        if check_win(tic) == player1_win
            println("Player Wins")
            break
        elseif check_win(tic) == player2_win
            println("AI wins.")
            break
        elseif check_win(tic) == draw
            println("Draw")
            break
        end

        ai_move!(tic, difficulty)
        displayGame(tic)

        if check_win(tic) == player1_win
            println("Player wins")
            break
        elseif check_win(tic) == player2_win
            println("AI wins.")
            break
        elseif check_win(tic) == draw
            println("Draw")
            break
        end
    end
end

function multiplayer(tic, (player1, player2), (player1_win, player2_win, draw))
    println("Player 1 is X, Player 2 is O. Player 1 goes first.")
    displayGame(tic)

    while true

        txt = move_input(tic, "Enter row and column seperated by space: ", 2)

        make_move!(tic, player1, (parse(Int, txt[1]), parse(Int, txt[2])))
        displayGame(tic)

        if check_win(tic) == player1_win
            println("Player 1 Wins!")
            break
        elseif check_win(tic) == player2_win
            println("Player 2 Wins!")
            break
        elseif check_win(tic) == draw
            println("Draw")
            break
        end

        txt = move_input(tic, "Enter row and column seperated by space", 2)

        make_move!(tic, player2, (parse(Int, txt[1]), parse(Int, txt[2])))

        displayGame(tic)


        if check_win(tic) == player1_win
            println("Player 1 Wins!")
            break
        elseif check_win(tic) == player2_win
            println("Player 2 Wins!")
            break
        elseif check_win(tic) == draw
            println("Draw")
            break
        end
    end
end


function play_again()
    replay = generalised_input("Play again? (y/n): ", true, ["y", "n"], 1)[1]
    if replay == "y"
        run(`julia $PROGRAM_FILE`)
        exit()
    else
        exit()
    end
end

mode = generalised_input("Welcome to TicTacToe! \n 1. Vs. AI  \n 2. Multiplayer \n", false, ["1", "2"], 1)[1]

if mode == "1"
    singleplayer(tic, (player1, player2), (player1_win, player2_win, draw))
else
    multiplayer(tic, (player1, player2), (player1_win, player2_win, draw))
end

play_again()
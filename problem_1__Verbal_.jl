### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 1fe7ae55-e096-4bde-9259-3dc7bb54bea8
using Pkg

# ╔═╡ f42a66a5-eba6-4a0b-b409-3786f98329bc
Pkg.activate("Project.toml")

# ╔═╡ 1fdec617-e08d-4929-83b3-8627ac397a8c
md"# AIG Assignment
	Problem 1 Verbal"

# ╔═╡ 7b97ee18-a286-11eb-0a70-3d4a10306477
#State structure
struct state
	name::String
	position::Int64
	officeW::Vector{Bool}
	officeC1::Vector{Bool}
	officeC2::Vector{Bool}
	officeE::Vector{Bool}
end

# ╔═╡ 4a1e2a8d-2f06-4d03-9d36-a48e332297ea
#Action structure
struct action
	name::String
	cost::Int64
end

# ╔═╡ 2ae27544-2d54-4630-b186-3fb55598ead7
me = action("Move East",3)

# ╔═╡ 195cdb07-67cc-4ed9-8df5-4f9bdde1937d
mw = action("Move West",3)

# ╔═╡ eb88f057-7b34-4b38-b87a-82a576bbdb4c
re = action("Remain",1)

# ╔═╡ 87e51205-5ec1-412e-8c03-59959bb6a068
co = action("Collect",5)

# ╔═╡ 869f4130-290b-4e23-9f45-8a8556da0b14
#initial state
state1 =("state1",2,[true,false,false],[true,true,true],[false,true,true],[true,false,false])

# ╔═╡ 519cf98d-044b-467e-9579-3a97ac354dca
#verbal in W
state2 =("state2",1,[true,false,false],[true,true,true],[false,true,true],[true,false,false])

# ╔═╡ acc39115-3f67-4963-bb07-6a492d983a46
#verbal in w collecting package
state3 =("state3",1,[false,false,false],[true,true,true],[false,true,true],[true,false,false])

# ╔═╡ c92ce9b7-70ef-4d68-9739-bb5027dfe2cf
#verbal in C1 package in w collected
state4 =("state4",2,[false,false,false],[true,true,true],[false,true,true],[true,false,false])

# ╔═╡ 995255c6-9000-4b04-88c8-036f243f8b73
#verba' in c2 package collected in w
state5 =("state5",3,[false,false,false],[true,true,true],[false,true,true],[true,false,false])

# ╔═╡ aafd541d-0ec9-458c-bcb2-1aeedb35924c
#verbal in e package collected in w
state6 =("state6",4,[false,false,false],[true,true,true],[false,true,true],[true,false,false])

# ╔═╡ 5c854e02-ac05-4f00-8ba4-ed74e739c5c0
#Verbal collecting packag in E
state7 =("state7",4,[false,false,false],[true,true,true],[false,true,true],[false,false,false])

# ╔═╡ 78b60932-4382-4044-8405-7f6873dc9fe5
#verbal in c2 with packages collected in w and e
state8 =("state8",3,[false,false,false],[true,true,true],[false,true,true],[false,false,false])

# ╔═╡ a7f22564-5c8a-4a86-ba7f-0efc1a2708bd
#verbal collecting package 1 in c2
state9 =("state9",3,[false,false,false],[true,true,true],[false,false,true],[false,false,false])

# ╔═╡ 08184fa6-3725-465a-8ba0-0c2a2f2f5128
#verbal collecting package 2 in c2
state10 =("state10",3,[false,false,false],[true,true,true],[false,false,false],[false,false,false])

# ╔═╡ a9c2eada-e4c5-4fba-b5b1-3935fd1ac0d6
#verbal in c1
state11 =("state11",2,[false,false,false],[true,true,true],[false,false,false],[false,false,false])

# ╔═╡ c822d1a4-bb85-418c-92a1-38d95e8eb739
#verbal collecting package 1 in c1
state12 =("state12",2,[false,false,false],[false,true,true],[false,false,false],[false,false,false])

# ╔═╡ 24428a62-2c99-4dc1-a51f-a49bbd26abd0
#verbal collecting package 1 in c1
state13 =("state13",2,[false,false,false],[false,false,true],[false,false,false],[false,false,false])

# ╔═╡ 98219d17-a300-43a5-91fa-87483fd6062b
#goalstate
state14 =("state14",2,[false,false,false],[false,false,false],[false,false,false],[false,false,false])

# ╔═╡ a7f1b567-2aba-4dd8-a2a5-2661be99b953
TModel =Dict()

# ╔═╡ bc35b034-1242-4cd1-8ac6-16f092daf0f7
push!(TModel,state1 => [(mw,state2)])

# ╔═╡ ff0bac36-f4c2-4812-acc2-8acaf8d6c557
push!(TModel,state2 => [(co,state3)])

# ╔═╡ 12b6ae13-eec3-49a3-8ad3-0959232d87db
push!(TModel,state3 => [(me,state4)])

# ╔═╡ 8dd918cd-3356-4ce2-ac6d-aed0c54f451b
push!(TModel,state4 => [(me,state5)])

# ╔═╡ b77e4a7a-87ff-42f0-9591-cc48931bc452
push!(TModel,state5 => [(me,state6)])

# ╔═╡ e08c7338-36ff-4261-adb1-d0f11af4a508
push!(TModel,state6 => [(co,state7)])

# ╔═╡ 74ca0ba6-5306-4b18-bb78-dd5ff860c08c
push!(TModel,state7 => [(me,state8)])

# ╔═╡ d218af5f-ba7a-4433-af50-c76dc38af592
push!(TModel,state9 => [(co,state9)])

# ╔═╡ c7232b88-17f5-4fc5-bf2d-57e6370c9c89
push!(TModel,state9 => [(co,state10)])

# ╔═╡ 7c2d7055-3623-488b-b9cf-8480633ae54e
push!(TModel,state10 => [(me,state11)])

# ╔═╡ 1196b001-0df5-4dc5-9f58-8aaf365030d4
push!(TModel,state11 => [(co,state12)])

# ╔═╡ 6d39eda1-4759-47b0-b88c-efadfb68d3fc
push!(TModel,state12 => [(co,state13)])

# ╔═╡ bab0f9c5-50d8-42fe-8583-7af192ca77a1
push!(TModel,state13 => [(co,state14)])

# ╔═╡ 10347a3f-18db-46a2-8ce8-cda5961675e4
goalstates = [state14]

# ╔═╡ 399ff37b-4d8e-4ad0-9181-ff9dc57674b4
TModel[state1]

# ╔═╡ 5ce64bdb-4575-47f5-b7d1-9e9fdb5f3abd
function CreateResult(TModel, explored, output)

2

    result = []

3

    for i in 1..(length(explored)-1)

4

        var1 = TModel[explored[i]]

5

        for element in var1

6

            if i == (length(explored)-1)

7

                if element[2] == output

8

                push!(result, element[1])

9

                end

10

            else

11

                if element[2] == explored[i+1]

12

                push!(result, element[1])

13

                end

14

            end

15

        end

16

    end

17

    return result

18

end

# ╔═╡ b80092ed-6e69-4c21-89d6-ca72d0dc620c
function Pathing(TModel, state14, goalstates)

2

    explored = []

3

    frontier = Queue{State}()

4

    enqueue!(frontier, Zero)

5

    while true

6

        if isempty(frontier)

7

            return []   

8

        else 

9

            CurrentState = dequeue!(frontier)

10

            push!(explored, CurrentState)

11

            NextStates = TModel[CurrentState]

12

            for element in NextStates

13

                #test is incomplete

14

                if element[2] not in explored

15

                    if element[2] in goalstates

16

                        return CreateResult(TModel, explored, element[2])

17

                    else

18

                        enqueue!(frontier, element[2])

19

                    end

20

                end

21

            end

22

        end

23

    end

24

end

# ╔═╡ ac002494-9227-449c-8662-111ee45defb0


# ╔═╡ Cell order:
# ╟─1fdec617-e08d-4929-83b3-8627ac397a8c
# ╠═1fe7ae55-e096-4bde-9259-3dc7bb54bea8
# ╠═f42a66a5-eba6-4a0b-b409-3786f98329bc
# ╠═7b97ee18-a286-11eb-0a70-3d4a10306477
# ╠═4a1e2a8d-2f06-4d03-9d36-a48e332297ea
# ╠═2ae27544-2d54-4630-b186-3fb55598ead7
# ╠═195cdb07-67cc-4ed9-8df5-4f9bdde1937d
# ╠═eb88f057-7b34-4b38-b87a-82a576bbdb4c
# ╠═87e51205-5ec1-412e-8c03-59959bb6a068
# ╠═869f4130-290b-4e23-9f45-8a8556da0b14
# ╠═519cf98d-044b-467e-9579-3a97ac354dca
# ╠═acc39115-3f67-4963-bb07-6a492d983a46
# ╠═c92ce9b7-70ef-4d68-9739-bb5027dfe2cf
# ╠═995255c6-9000-4b04-88c8-036f243f8b73
# ╠═aafd541d-0ec9-458c-bcb2-1aeedb35924c
# ╠═5c854e02-ac05-4f00-8ba4-ed74e739c5c0
# ╠═78b60932-4382-4044-8405-7f6873dc9fe5
# ╠═a7f22564-5c8a-4a86-ba7f-0efc1a2708bd
# ╠═08184fa6-3725-465a-8ba0-0c2a2f2f5128
# ╠═a9c2eada-e4c5-4fba-b5b1-3935fd1ac0d6
# ╠═c822d1a4-bb85-418c-92a1-38d95e8eb739
# ╠═24428a62-2c99-4dc1-a51f-a49bbd26abd0
# ╠═98219d17-a300-43a5-91fa-87483fd6062b
# ╠═a7f1b567-2aba-4dd8-a2a5-2661be99b953
# ╠═bc35b034-1242-4cd1-8ac6-16f092daf0f7
# ╠═ff0bac36-f4c2-4812-acc2-8acaf8d6c557
# ╠═12b6ae13-eec3-49a3-8ad3-0959232d87db
# ╠═8dd918cd-3356-4ce2-ac6d-aed0c54f451b
# ╠═b77e4a7a-87ff-42f0-9591-cc48931bc452
# ╠═e08c7338-36ff-4261-adb1-d0f11af4a508
# ╠═74ca0ba6-5306-4b18-bb78-dd5ff860c08c
# ╠═d218af5f-ba7a-4433-af50-c76dc38af592
# ╠═c7232b88-17f5-4fc5-bf2d-57e6370c9c89
# ╠═7c2d7055-3623-488b-b9cf-8480633ae54e
# ╠═1196b001-0df5-4dc5-9f58-8aaf365030d4
# ╠═6d39eda1-4759-47b0-b88c-efadfb68d3fc
# ╠═bab0f9c5-50d8-42fe-8583-7af192ca77a1
# ╠═10347a3f-18db-46a2-8ce8-cda5961675e4
# ╠═399ff37b-4d8e-4ad0-9181-ff9dc57674b4
# ╟─b80092ed-6e69-4c21-89d6-ca72d0dc620c
# ╟─5ce64bdb-4575-47f5-b7d1-9e9fdb5f3abd
# ╠═ac002494-9227-449c-8662-111ee45defb0
